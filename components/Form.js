import React, { useState } from 'react';
import { Text, TextInput, View, StyleSheet, TouchableWithoutFeedback, Animated, Platform, Alert, TouchableOpacity } from 'react-native';
import { Picker } from '@react-native-community/picker';


const Form = ({search, setSearch, setConsult}) => {

    const {country, city} = search;

    const checkWeather = () => {
        if(city.trim() === '' || country.trim() === ''){
            showAlert();
            return;
        }

        // Consult the API
        setConsult(true);
        console.log(search)
    }

    const showAlert = () => {
        Alert.alert(
            'Error',
            'All the fields are required',
            [{
                text: 'OK'
            }]
        )
    }

    const [btnAnimation] = useState(new Animated.Value(1));

    const enterAnimation = () => {
        Animated.spring(btnAnimation, {
            toValue: .8,
            useNativeDriver: true,   //OJO!
            delay: 0,
            //    restSpeedThreshold: 1000,
            //    restDisplacementThreshold: 400,
            //    velocity: 1,
            //    duration: 10,
        }).start(function onComplete() {
            exitAnimation()
        });
        console.log('start')
        
        // setTimeout(() => {
        //     exitAnimation();
        // }, 1)
    }

    const exitAnimation = () => {
        Animated.spring(btnAnimation, {
            toValue: 1,
            friction: 4,    // Controls the rebound more friction - more rebound
            tension: 300,   // The lower the number, smoothest the animation 
            useNativeDriver: true,    //OJO!
            delay: 0
            //     restSpeedThreshold: 1000,
            //    restDisplacementThreshold: 400,
            // duration: 0,
        }).start();
        console.log('exit')
    }

    const animationStyles = {
        transform: [{ scale: btnAnimation }]
    }


    return (
        <>
            <View style={styles.form}>
                <View>
                    <TextInput
                        value= {city}
                        style={styles.input}
                        onChangeText= {city => setSearch({...search, city})}
                        placeholder="City"
                        placeholderTextColor="#666"
                    />
                </View>
                <View style={Platform.OS !== 'ios' ? styles.viewPicker : {}}>
                    <Picker
                        style={Platform.OS === 'ios' ? styles.pickerIOS : styles.pickerAND}
                        selectedValue={country}
                        onValueChange = {country => setSearch({...search, country}) }
                    >
                        <Picker.Item label="-- Select a Country --" value="" />
                        <Picker.Item label="USA" value="US" />
                        <Picker.Item label="Mexico" value="MX" />
                        <Picker.Item label="Spain" value="ES" />
                        <Picker.Item label="Argentina" value="AR" />
                        <Picker.Item label="Colombia" value="CO" />
                        <Picker.Item label="Costa Rica" value="CR" />
                        <Picker.Item label="Peru" value="PE" />
                    </Picker>
                </View>
                <TouchableWithoutFeedback
                    onPress={() => checkWeather()}
                    delayPressIn={0}
                    onPressIn={() => enterAnimation()}
                    // delayPressOut={0}
                    // onPressOut={() => exitAnimation()}
                    // activeOpacity={0.5}
                    >
                    <Animated.View
                        style={[styles.btnSearch, animationStyles]}
                    >
                        <Text style={styles.textSearch}>Search Weather</Text>
                    </Animated.View>
                </TouchableWithoutFeedback>
            </View>
        </>
    );
}

const styles = StyleSheet.create({
    input: {
        padding: 10,
        height: 50,
        backgroundColor: "#FFF",
        fontSize: 20,
        marginBottom: 20,
        textAlign: 'center'
    },
    pickerIOS: {
        height: 120,
        backgroundColor: '#FFF'
    },
    pickerAND: {
        height: 50,
        // color: '#FFF',
    },
    viewPicker: {
        backgroundColor: '#FFF'
    },
    btnSearch: {
        marginTop: 50,
        backgroundColor: '#000',
        padding: 10,
        justifyContent: 'center'

    },
    textSearch: {
        color: '#FFF',
        fontWeight: 'bold',
        textTransform: 'uppercase',
        textAlign: 'center',
        fontSize: 18
    }
})

export default Form;