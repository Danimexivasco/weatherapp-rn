import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native'
const Weather = ({ result }) => {

    const { name, main } = result;

    if (!name) return null;

    // Kelvin degrees
    const kelvin = 273.15;

    console.log(result)
    return (
        <>
            <View style={styles.weather}>
                <Text style={[styles.text, styles.actual]}>
                    {parseInt(main.temp - kelvin)}
                    {/* <Text style={styles.temperature}>
                    &#x2103;
                </Text> */}
                </Text>
                <Text style={styles.temperature}>
                    &#x2103;
            </Text>
                <Image
                    style={{ width: 80, height: 80 }}
                    source={{ uri: `http://openweathermap.org/img/w/${result.weather[0].icon}.png` }}
                />
            </View>
            <View style={styles.temperatures}>
                <Text style={styles.text}> Min {' '}
                    <Text style={styles.temperature}>
                        {parseInt(main.temp_min - kelvin)} &#x2103;
                    </Text>
                </Text>

                <Text style={styles.text}> Max {' '}
                    <Text style={styles.temperature}>
                        {parseInt(main.temp_max - kelvin)} &#x2103;
                    </Text>
                </Text>
            </View>
        </>
    );
}

const styles = StyleSheet.create({
    weather: {
        // marginBottom: 20,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'

    },
    text: {
        color: '#FFF',
        fontSize: 20,
        textAlign: 'center',
        marginRight: 20
    },
    actual: {
        fontSize: 80,
        marginRight: 0,
        fontWeight: 'bold',

    },
    temperature: {
        fontSize: 30,
        fontWeight: 'normal',
        color: '#FFF',
        paddingBottom: 30,
    },
    temperatures: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: 20
    }
})

export default Weather;