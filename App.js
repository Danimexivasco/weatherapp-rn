
import React, { useState, useEffect } from 'react';
import { StyleSheet, View, TouchableWithoutFeedback, Keyboard, Alert } from 'react-native';
import { API_KEY } from '@env';

import Form from './components/Form';
import Weather from './components/Weather';


const App = () => {

  const [search, setSearch] = useState({
    city: '',
    country: ''
  });
  const [consult, setConsult] = useState(false);
  const [result, setResult] = useState({});
  const [bgColor, setBgColor] = useState('rgb(71, 149, 212)')

  // Destructuring Search
  const { city, country } = search;

  useEffect(() => {
    const consultWeather = async () => {
      if (consult) {
        const apiKey = `${API_KEY}`;
        const url = `http://api.openweathermap.org/data/2.5/weather?q=${city.trim()},${country}&appid=${apiKey}`;
        try {
          const response = await fetch(url);
          const result = await response.json();
          setResult(result);
          setConsult(false);

          // Modify bgcolor
          const kelvin = 273.15;
          const {main} = result;
          const actual = main.temp - kelvin;

          if(actual < 10){
            setBgColor('rgb(105, 108, 149)')
          }else if(actual >= 10 && actual < 25){
            setBgColor('rgb(71, 149, 212)')
          }else{
            setBgColor('rgb(178, 28,61)')
          }


        } catch (error) {
          showAlert();
          console.error(error)
        }
      }
    }
    consultWeather();
  }, [consult])

  const showAlert = () => {
    Alert.alert(
      'Error',
      'There are not results for this search. Try with another inputs',
      [{
        text: 'OK'
      }]
    )
  }

  const hideKeyboard = () => {
    Keyboard.dismiss();
  }

  const bgColorApp = {
    backgroundColor: bgColor
  }


  return (
    <>
      <TouchableWithoutFeedback onPress={() => hideKeyboard()}>
        <View style={[styles.app, bgColorApp]}>
          <View style={styles.container}>
            <Weather
              result={result}
            />
            <Form
              search={search}
              setSearch={setSearch}
              setConsult={setConsult}
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
    </>
  );
};

const styles = StyleSheet.create({
  app: {
    flex: 1,
    // backgroundColor: 'rgb(71, 149, 212)',
    justifyContent: 'center'
  },
  container: {
    marginHorizontal: '2.5%'
  },

});

export default App;
